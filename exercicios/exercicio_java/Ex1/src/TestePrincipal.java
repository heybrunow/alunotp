

import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bruno
 */
public class TestePrincipal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Paciente p1, p2;

        p1 = new Paciente();

        p1.nome = JOptionPane.showInputDialog(null, "Digite o nome do paciente 1");
        p1.rg = JOptionPane.showInputDialog(null, "Digite o rg do paciente 1");
        p1.endereco = JOptionPane.showInputDialog(null, "Digite o endereco do paciente 1");
        p1.telefone = JOptionPane.showInputDialog(null, "Digite o telefone do paciente 1");
        p1.dataNascimento = JOptionPane.showInputDialog(null, "Digite a data de nascimento do paciente 1");
        p1.profissao = JOptionPane.showInputDialog(null, "Digite a profissao do paciente 1");

        JOptionPane.showMessageDialog(null, "O nome do paciente é: " + p1.nome
                + "\nO rg do paciente é: " + p1.rg
                + "\nO endereco do paciente é: " + p1.endereco
                + "\nO telefone do paciente é: " + p1.telefone
                + "\nA data de nascimento do paciente é: " + p1.dataNascimento
                + "\nA profissao do paciente é: " + p1.profissao);

        p2 = new Paciente("josias");

        //p2.nome = JOptionPane.showInputDialog(null, "Digite o nome do paciente 2");
        p2.rg = JOptionPane.showInputDialog(null, "Digite o rg do paciente 2");
        p2.endereco = JOptionPane.showInputDialog(null, "Digite o endereco do paciente 2");
        p2.telefone = JOptionPane.showInputDialog(null, "Digite o telefone do paciente 2");
        p2.dataNascimento = JOptionPane.showInputDialog(null, "Digite a data de nascimento do paciente 2");
        p2.profissao = JOptionPane.showInputDialog(null, "Digite a profissao do paciente 2");

        JOptionPane.showMessageDialog(null, "O nome do paciente é: " + p2.nome
                + "\nO rg do paciente é: " + p2.rg
                + "\nO endereco do paciente é: " + p2.endereco
                + "\nO telefone do paciente é: " + p2.telefone
                + "\nA data de nascimento do paciente é: " + p2.dataNascimento
                + "\nA profissao do paciente é: " + p2.profissao);

    }

}
